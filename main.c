//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "main.h"
#include <delay.h>
#include <disp1color.h>
#include <ssd1306multi.h>
#include <font.h>

#define delay   150



uint16_t TimeCounter = 0;



void UpdateDisplay(uint8_t Num)
{
  disp1color_SelectDisplay(Num);
  disp1color_FillScreenbuff(0);
  // ����� ��� ����������� 
  disp1color_DrawRectangle(0, 0, 127, 63); 
  // ����������� (� ������ ���������������� ������ �� �������) 
  uint8_t X = Num >= 9 ? 1 : 6;
  disp1color_printf(X, 4, FONTID_32F, "%d", Num + 1);
  disp1color_printf(28, 2, FONTID_16F, "�����������\n\r� ���������");

  for (uint8_t i = 0; i < 8; i++)
  {
    if (i >= TimeCounter)
      disp1color_DrawRectangle(i * 16, 36, (i * 16) + 15, 49);
    else
      disp1color_DrawRectangleFilled(i * 16, 36, (i * 16) + 15, 49);
  }
  for (uint8_t i = 0; i < 8; i++)
  {
    if ((i + 8) >= TimeCounter)
      disp1color_DrawRectangle(i * 16, 50, (i * 16) + 15, 63); 
    else
      disp1color_DrawRectangleFilled(i * 16, 50, (i * 16) + 15, 63); 
  }
  
  // ������� �������������� �������� �� �������
  disp1color_UpdateFromBuff();
}


void main()
{
  SystemInit();
  delay_ms(500);
  
  // ������������� ��������
  disp1color_Init();

  // ������������� ������� ��������
  for (int i = 0; i < DISP1COLOR_DisplayNum; i++)
  {
    disp1color_SelectDisplay(i);
    disp1color_SetBrightness(255);
  }

  // ��-������� �������� ������� ��������� (���� ����� ssd1306)
  for (int i = 0; i < DISP1COLOR_DisplayNum; i++)
  {
    disp1color_SelectDisplay(i);
    disp1color_TestMode(1);
    delay_ms(1000);
    disp1color_TestMode(0);
  }
/*  // ��� ���� ��� ������� �����
  delay_ms(5000);
  // ����� ��� ������� ����� (������� ssd1306 �� ������ ����)
  for (int i = 0; i < DISP1COLOR_DisplayNum; i++)
  {
    disp1color_SelectDisplay(i);
    disp1color_TestMode(0);
  }
  */
  while (1)
  {
    // ������������ ��������� �������� �� ��������
    for (int i = 0; i < DISP1COLOR_DisplayNum; i++)
      UpdateDisplay(i);

    // ����������� ������� ������� ��� ����������� ������ �����
    if (++TimeCounter > 16)
      TimeCounter = 0;

    delay_ms(1000);
  }
}
