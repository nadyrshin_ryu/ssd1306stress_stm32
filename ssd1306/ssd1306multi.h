//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _SSD1306MULTI_H
#define _SSD1306MULTI_H

#include <types.h>


// ������ ��������������� ������ ��������� � ������ ����� ssd1306
#define SSD1306_Adressing_Horizontal            0       // ������� ��������� �� �����������, ����� ��������� �� ���������
#define SSD1306_Adressing_Vertical              1       // ������� ��������� �� ���������, ����� ��������� �� �����������
#define SSD1306_Adressing_Page                  2       // ��������� ������ �� �����������


//==============================================================================
// ���� ������ �������
//==============================================================================
// ������� �� ������� Fundamental
#define SSD1306_CMD_SetContrast                 0x81
#define SSD1306_CMD_AllPixRAM                   0xA4
#define SSD1306_CMD_AllPixOn                    0xA5
#define SSD1306_CMD_SetInverseOff               0xA6
#define SSD1306_CMD_SetInverseOn                0xA7
#define SSD1306_CMD_Sleep                       0xAE
#define SSD1306_CMD_Wake                        0xAF

// ������� �� ������� Scrolling
#define SSD1306_CMD_DeactivateScroll            0x2E

// ������� �� ������� Addressing Setting
// ������� ������ ������ ���������� ��������� � ������ �����
#define SSD1306_CMD_SetMemAdressingMode         0x20    
// ������� ������ ��������� ��������� �������� � ������� ��� ���������� ��������� � ������ �����
// ����������� ��� ������� ���������� SSD1306_Adressing_Horizontal � SSD1306_Adressing_Vertical
#define SSD1306_CMD_SetColumnAddr               0x21
#define SSD1306_CMD_SetPageAddr                 0x22
// ������� ������ �������� � ��������� ��������� ������� ��� ���������� ��������� � ������ �����
// ����������� ��� ������ ���������� SSD1306_Adressing_Page
#define SSD1306_CMD_PageAddrMode_SetPage        0xB0
#define SSD1306_CMD_PageAddrMode_StartColumnLo  0x00
#define SSD1306_CMD_PageAddrMode_StartColumnHi  0x10

// ������� �� ������� Hardware Configuration
#define SSD1306_CMD_SetDisplayStartLine         0x40
#define SSD1306_CMD_SetSegmentRemap             0xA0
#define SSD1306_CMD_SetMultiplexRatio           0xA8
#define SSD1306_CMD_SetCOMoutScanDirection      0xC0 
#define SSD1306_CMD_SetDisplayOffset            0xD3
#define SSD1306_CMD_SetCOMPinsConfig            0xDA
  
// ������� �� ������� Timing & Driving Scheme Setting
#define SSD1306_CMD_SetDisplayClockDivider      0xD5
#define SSD1306_CMD_SetPrechargePeriod          0xD9
#define SSD1306_CMD_SetVCOMHDeselectLevel       0xDB

// ������� �� ������� Charge Pump
#define SSD1306_CMD_ChargePumpSetting           0x8D
//==============================================================================


//==============================================================================
// ��������� ����������� � ��������
//==============================================================================
#define SSD1306_DisplayNum            11

#define SSD1306_ResetPinUsed          0       // ������������ ���������� ����� Reset ssd1306      
#define SSD1306_Reset_Port            GPIOB
#define SSD1306_Reset_Pin             GPIO_Pin_9

#define SSD1306_SPI_periph            SPI1    // SPI1 ��� SPI2        
#define SSD1306_CSPinUsed             1       // ������������ ���������� ����� CS ssd1306      
#define SSD1306_DC_Port               GPIOB
#define SSD1306_DC_Pin                GPIO_Pin_12
//==============================================================================


// ��������� ������������� ������� �� ����������� ssd1306
void SSD1306_Init(uint8_t Width, uint8_t Height);
// ��������� ��������� ������� � ����� ���
void SSD1306_Sleep(uint8_t DisplayNum);
// ��������� ������� ������� �� ������ ���
void SSD1306_Wake(uint8_t DisplayNum);
// ��������� �������� �������� �������
void SSD1306_SetInverseOn(uint8_t DisplayNum);
// ��������� ��������� �������� �������
void SSD1306_SetInverseOff(uint8_t DisplayNum);
// ��������� �������� ��� ������� ������� (���� ����������)
void SSD1306_AllPixOn(uint8_t DisplayNum);
// ��������� ��������� ���� ������� � ������� �� ���� �������� �� ������ ����� � ssd1306
void SSD1306_AllPixRAM(uint8_t DisplayNum);
// ��������� ������������� �������� ������������� (0-255)
void SSD1306_SetContrast(uint8_t DisplayNum, uint8_t Value);
// ��������� ������������� ��������� � �������� ������ ������� 
// ��� ������������ ��������� � ������ ����� ��� ������ ������.
void SSD1306_SetColumns(uint8_t DisplayNum, uint8_t Start, uint8_t End);
// ��������� ������������� ��������� � �������� ������ �������� 
// ��� ������������ ��������� � ������ ����� ��� ������ ������.
void SSD1306_SetPages(uint8_t DisplayNum, uint8_t Start, uint8_t End);
// ��������� ������� � ������� ����� ����� �� ������� pBuff
void SSD1306_DisplayFullUpdate(uint8_t DisplayNum, uint8_t *pBuff, uint16_t BuffLen);

#endif
