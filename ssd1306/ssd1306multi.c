//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stm32f10x_gpio.h>
#include <stm32f10x_rcc.h>
#include <delay.h>
#include <ssd1306multi.h>
#include <gpio.h>
#include <i2cm.h>
#include <spim.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>


#define SSD1306_RESET_HIGH()    GPIO_WriteBit(SSD1306_Reset_Port, SSD1306_Reset_Pin, Bit_SET)
#define SSD1306_RESET_LOW()     GPIO_WriteBit(SSD1306_Reset_Port, SSD1306_Reset_Pin, Bit_RESET)
#define SSD1306_DC_HIGH()       GPIO_WriteBit(SSD1306_DC_Port, SSD1306_DC_Pin, Bit_SET)
#define SSD1306_DC_LOW()        GPIO_WriteBit(SSD1306_DC_Port, SSD1306_DC_Pin, Bit_RESET)

#if (SSD1306_CSPinUsed)
  // ���������, ����������� ���� ���� gpio
  typedef struct
  {
    GPIO_TypeDef *port;
    uint16_t pin;
  } tCSpin;
  
  // ��������� ��� CS �������� 
  static tCSpin CSpins[SSD1306_DisplayNum];

  #define SSD1306_CS_HIGH(num)     GPIO_WriteBit(CSpins[num].port, CSpins[num].pin, Bit_SET)
  #define SSD1306_CS_LOW(num)      GPIO_WriteBit(CSpins[num].port, CSpins[num].pin, Bit_RESET)
#else
  #define SSD1306_CS_HIGH(num)     ;
  #define SSD1306_CS_LOW(num)      ;
#endif



// ������� ������� � ��������
static uint8_t SSD1306_Height;
static uint8_t SSD1306_Width;


//==============================================================================
// ��������� ������� ������� � ����������� � ssd1306
//==============================================================================
void SSD1306_SendCommand(uint8_t DisplayNum, uint8_t Cmd, uint8_t *pBuff, uint16_t BuffLen)
{
  SSD1306_DC_LOW();
  SSD1306_CS_LOW(DisplayNum);
  SPI_send8b(SSD1306_SPI_periph, &Cmd, 1);
  SPI_send8b(SSD1306_SPI_periph, pBuff, BuffLen);
  SSD1306_CS_HIGH(DisplayNum);
}
//==============================================================================


//==============================================================================
// ��������� ������� ������ ������ � ssd1306
//==============================================================================
void SSD1306_SendData(uint8_t DisplayNum, uint8_t *pBuff, uint16_t BuffLen)
{
  SSD1306_DC_HIGH();
  SSD1306_CS_LOW(DisplayNum);
  SPI_send8b(SSD1306_SPI_periph, pBuff, BuffLen);
  SSD1306_CS_HIGH(DisplayNum);
  SSD1306_DC_LOW();
}
//==============================================================================


//==============================================================================
// ��������� ������� � ������� ����� ����� �� ������� pBuff
//==============================================================================
void SSD1306_DisplayFullUpdate(uint8_t DisplayNum, uint8_t *pBuff, uint16_t BuffLen)
{
  SSD1306_SetColumns(DisplayNum, 0, SSD1306_Width - 1);
  SSD1306_SetPages(DisplayNum, 0, (SSD1306_Height >> 3) - 1);
  SSD1306_SendData(DisplayNum, pBuff, BuffLen);
}
//==============================================================================


//==============================================================================
// ��������� ������������� ����� ���������� ��������� � ������ ����� ssd1306
//==============================================================================
void SSD1306_SetMemAdressingMode(uint8_t DisplayNum, uint8_t Mode)
{
  if (Mode > 2)
    return;
  
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_SetMemAdressingMode | Mode , 0, 0);
}
//==============================================================================


//==============================================================================
// ��������� ������������� ��������� � �������� ������ ������� 
// ��� ������������ ��������� � ������ ����� ��� ������ ������.
//==============================================================================
void SSD1306_SetColumns(uint8_t DisplayNum, uint8_t Start, uint8_t End)
{
  Start &= 0x7F;
  End &= 0x7F;
  uint8_t Buff[] = {Start, End};
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_SetColumnAddr, Buff, 2);
}
//==============================================================================


//==============================================================================
// ��������� ������������� ��������� � �������� ������ �������� 
// ��� ������������ ��������� � ������ ����� ��� ������ ������.
//==============================================================================
void SSD1306_SetPages(uint8_t DisplayNum, uint8_t Start, uint8_t End)
{
  Start &= 0x07;
  End &= 0x07;
  uint8_t Buff[] = {Start, End};
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_SetPageAddr, Buff, 2);
}
//==============================================================================


//==============================================================================
// ��������� ������������� �������� ��� ������ ���������� �����������, �����
// ����� ��������� � ������ �������� ������ �� ����������� (SSD1306_Adressing_Page).
//==============================================================================
void SSD1306_PageAddrMode_SetPage(uint8_t DisplayNum, uint8_t Page)
{
  Page &= 0x07;
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_PageAddrMode_SetPage | Page, 0, 0);
}
//==============================================================================


//==============================================================================
// ��������� ������������� � ������, �� ������� ��������� � ������ ����� ����� ������������
// ������ ��� ������ ���������� ��������� (SSD1306_Adressing_Page)
//==============================================================================
void SSD1306_PageAddrMode_StartColumn(uint8_t DisplayNum, uint8_t Start)
{
  Start &= 0x7F;
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_PageAddrMode_StartColumnLo | (Start & 0x07), 0, 0);
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_PageAddrMode_StartColumnHi | (Start >> 4), 0, 0);
}
//==============================================================================


//==============================================================================
// Set display RAM display start line register from 0-63
//==============================================================================
void SSD1306_SetDisplayStartLine(uint8_t DisplayNum, uint8_t Line)
{
  Line &= 0x3F;
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_SetDisplayStartLine | Line, 0, 0);
}
//==============================================================================


//==============================================================================
// Value=0: column address 0 is mapped to SEG0 (RESET)
// Value=1: column address 127 is mapped to SEG0 
//==============================================================================
void SSD1306_SetSegmentRemap(uint8_t DisplayNum, uint8_t Value)
{
  Value = Value ? 1 : 0;
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_SetSegmentRemap | Value, 0, 0);
}
//==============================================================================


//==============================================================================
void SSD1306_SetMultiplexRatio(uint8_t DisplayNum, uint8_t Mux)
{
  Mux--;
  Mux &= 0x3F;
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_SetMultiplexRatio, &Mux, 1);
}
//==============================================================================


//==============================================================================
// �������� �� ��������� ������� ����������� ������������ ������ �����
//==============================================================================
void SSD1306_SetDisplayOffset(uint8_t DisplayNum, uint8_t Offset)
{
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_SetDisplayOffset, &Offset, 1);
}
//==============================================================================


//==============================================================================
void SSD1306_SetDisplayClockDivider(uint8_t DisplayNum, uint8_t DCLKdiv, uint8_t Fosc)
{
  DCLKdiv--;
  DCLKdiv &= 0x0F;
  DCLKdiv |= ((Fosc & 0x0F) << 4);
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_SetDisplayClockDivider, &DCLKdiv, 1);
}
//==============================================================================


//==============================================================================
void SSD1306_ChargePumpSetting(uint8_t DisplayNum, uint8_t Value)
{
  Value = Value ? 0x14 : 0x10;
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_ChargePumpSetting, &Value, 1);
}
//==============================================================================


//==============================================================================
// ����������� ������������
// Value=0: normal mode (RESET) Scan from COM0 to COM[N �1]
// Value=1: remapped mode. Scan from COM[N-1] to COM0
// Where N is the Multiplex ratio. 
//==============================================================================
void SSD1306_SetCOMoutScanDirection(uint8_t DisplayNum, uint8_t Value)
{
  Value = Value ? 0x08 : 0x00;
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_SetCOMoutScanDirection | Value, 0, 0);
}
//==============================================================================


//==============================================================================
// AltCOMpinConfig=0: Sequential COM pin configuration
// AltCOMpinConfig=1(RESET): Alternative COM pinconfiguration
// LeftRightRemap=0(RESET): Disable COM Left/Right remap
// LeftRightRemap=1: Enable COM Left/Right remap 
//==============================================================================
void SSD1306_SetCOMPinsConfig(uint8_t DisplayNum, uint8_t AltCOMpinConfig, uint8_t LeftRightRemap)
{
  uint8_t tmpValue = (1 << 1);
  if (AltCOMpinConfig)
    tmpValue |= (1 << 4);
  if (LeftRightRemap)
    tmpValue |= (1 << 5);
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_SetCOMPinsConfig, &tmpValue, 1);
}
//==============================================================================


//==============================================================================
void SSD1306_SetPrechargePeriod(uint8_t DisplayNum, uint8_t Phase1period, uint8_t Phase2period)
{
  Phase1period &= 0x0F;
  Phase1period &= 0x0F;
  if (!Phase1period)
    Phase1period = 2;
  if (!Phase2period)
    Phase2period = 2;
  Phase1period |= (Phase2period << 4);
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_SetPrechargePeriod, &Phase1period, 1);
}
//==============================================================================


//==============================================================================
void SSD1306_SetVCOMHDeselectLevel(uint8_t DisplayNum, uint8_t Code)
{
  Code &= 0x70;
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_SetVCOMHDeselectLevel, &Code, 1);
}
//==============================================================================


//==============================================================================
// ��������� ��������� ������ (���������� ����������� �������
//==============================================================================
void SSD1306_DeactivateScroll(uint8_t DisplayNum)
{
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_DeactivateScroll, 0, 0);
}
//==============================================================================


//==============================================================================
// ��������� ��������� ������� � ����� ���
//==============================================================================
void SSD1306_Sleep(uint8_t DisplayNum)
{
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_Sleep, 0, 0);
}
//==============================================================================


//==============================================================================
// ��������� ������� ������� �� ������ ���
//==============================================================================
void SSD1306_Wake(uint8_t DisplayNum)
{
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_Wake, 0, 0);
}
//==============================================================================


//==============================================================================
// ��������� �������� ��� ������� ������� (���� ����������)
//==============================================================================
void SSD1306_AllPixOn(uint8_t DisplayNum)
{
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_AllPixOn, 0, 0);
}
//==============================================================================


//==============================================================================
// ��������� ��������� ���� ������� � ������� �� ���� �������� �� ������ ����� � ssd1306
//==============================================================================
void SSD1306_AllPixRAM(uint8_t DisplayNum)
{
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_AllPixRAM, 0, 0);
}
//==============================================================================


//==============================================================================
// ��������� �������� �������� �������
//==============================================================================
void SSD1306_SetInverseOn(uint8_t DisplayNum)
{
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_SetInverseOn, 0, 0);
}
//==============================================================================


//==============================================================================
// ��������� ��������� �������� �������
//==============================================================================
void SSD1306_SetInverseOff(uint8_t DisplayNum)
{
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_SetInverseOff, 0, 0);
}
//==============================================================================


//==============================================================================
// ��������� ������������� �������� ������������� (0-255)
//==============================================================================
void SSD1306_SetContrast(uint8_t DisplayNum, uint8_t Value)
{
  SSD1306_SendCommand(DisplayNum, SSD1306_CMD_SetContrast, &Value, 1);
}
//==============================================================================


//==============================================================================
// ��������� ��������� ����� ���������������� ��� ������ � ��������
//==============================================================================
void SSD1306_GPIO_init(void)
{
  // ���������� ����, � ������� ���������� CS ��������
  CSpins[0].port = GPIOB;
  CSpins[0].pin = (1 << 0);
  CSpins[1].port = GPIOB;
  CSpins[1].pin = (1 << 1);
  CSpins[2].port = GPIOB;
  CSpins[2].pin = (1 << 10);
  CSpins[3].port = GPIOB;
  CSpins[3].pin = (1 << 11);
  CSpins[4].port = GPIOA;
  CSpins[4].pin = (1 << 11);
  CSpins[5].port = GPIOA;
  CSpins[5].pin = (1 << 10);
  CSpins[6].port = GPIOA;
  CSpins[6].pin = (1 << 9);
  CSpins[7].port = GPIOB;
  CSpins[7].pin = (1 << 8);
  CSpins[8].port = GPIOB;
  CSpins[8].pin = (1 << 7);
  CSpins[9].port = GPIOB;
  CSpins[9].pin = (1 << 6);
  CSpins[10].port = GPIOB;
  CSpins[10].pin = (1 << 5);
  
  GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
  
#if (SSD1306_ResetPinUsed)      // �������� ���������� ������ reset ssd1306
  gpio_PortClockStart(SSD1306_Reset_Port);
  GPIO_InitStruct.GPIO_Pin = SSD1306_Reset_Pin;
  GPIO_Init(SSD1306_Reset_Port, &GPIO_InitStruct);
#endif
  
#if (SSD1306_CSPinUsed)
  for (int i = 0; i < SSD1306_DisplayNum; i++)
  {
    gpio_PortClockStart(CSpins[i].port);
    GPIO_InitStruct.GPIO_Pin = CSpins[i].pin;
    GPIO_Init(CSpins[i].port, &GPIO_InitStruct);
    
  }

  //GPIO_PinRemapConfig(GPIO_Remap_SPI1, DISABLE);

   //GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, DISABLE);
#endif
  
  gpio_PortClockStart(SSD1306_DC_Port);
  GPIO_InitStruct.GPIO_Pin = SSD1306_DC_Pin;
  GPIO_Init(SSD1306_DC_Port, &GPIO_InitStruct);
}
//==============================================================================


//==============================================================================
// ��������� ������������� ������� �� ����������� ssd1306
//==============================================================================
void SSD1306_Init(uint8_t Width, uint8_t Height)
{
  SSD1306_Width = Width;
  SSD1306_Height = Height;
  
  SSD1306_GPIO_init();
  
  // ������������� ����������
  spim_init(SSD1306_SPI_periph, 8);

  // ����� ����������� ������� ssd1306 ������ Reset
#if (SSD1306_ResetPinUsed)      // �������� ���������� ������ reset ssd1306
  SSD1306_RESET_HIGH();
  delay_ms(2);
  SSD1306_RESET_LOW();  // ������ ����� reset � 0 �� 10 ��
  delay_ms(15);
  SSD1306_RESET_HIGH();
#endif
  
  // ��� ������� ������������� ssd1306
  for (int i = 0; i < SSD1306_DisplayNum; i++)
  {
    SSD1306_Sleep(i);
    SSD1306_SetDisplayClockDivider(i, 1, 8);
    SSD1306_SetMultiplexRatio(i, SSD1306_Height);
    SSD1306_SetDisplayOffset(i, 0);
    SSD1306_SetDisplayStartLine(i, 0);
    SSD1306_ChargePumpSetting(i, 1);
    SSD1306_SetMemAdressingMode(i, SSD1306_Adressing_Horizontal);
    SSD1306_SetSegmentRemap(i, 0);           // *������ ����������� ���������� ������� �� ������ ����� (���������/�����������)
    SSD1306_SetCOMoutScanDirection(i, 0);    // *�������������� ������������ �� ������� (������ �� ���������)
  
    if ((SSD1306_Width == 128) && (SSD1306_Height == 32))
      SSD1306_SetCOMPinsConfig(i, 0, 0);
    else  if ((SSD1306_Width == 128) && (SSD1306_Height == 64))
      SSD1306_SetCOMPinsConfig(i, 1, 0);
    else  if ((SSD1306_Width == 96) && (SSD1306_Height == 16))
      SSD1306_SetCOMPinsConfig(i, 0, 0);
  
    SSD1306_SetContrast(i, 127);
    SSD1306_SetPrechargePeriod(i, 2, 2);
    SSD1306_SetVCOMHDeselectLevel(i, 0x40);
    SSD1306_AllPixRAM(i);
    SSD1306_SetInverseOff(i);
    SSD1306_DeactivateScroll(i);
    SSD1306_Wake(i);
  }
}
//==============================================================================
